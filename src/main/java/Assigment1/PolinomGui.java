package PT2017.demo.DemoProject;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.activity.InvalidActivityException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

@SuppressWarnings("serial")

public class PolinomGui extends JFrame {

	private JFrame frame;
	private JTextField txtInsertPolinom1;
	private JTextField txtInsertPolinom2;
	private JLabel labelP;
	private JLabel labelQ;
	private JLabel labelError;
	private JLabel labelInstruction;
	private JLabel displayAdd;
	private JLabel displaySubstract;
	private JLabel displayMultiplication;
	private JLabel displayDerivation;
	private JLabel displayIntegration;
	private JButton btnAddPolinom;
	private JButton btnSubstractPolinom;
	private JButton btnMultiplyPolinom;
	private JButton btnDerivatePolinom;
	private JButton btnIntegratePolinom;

	private Polinom p1 = new Polinom();
	private Polinom p2 = new Polinom();

	public PolinomGui() {

		frame = new JFrame();

		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setTitle("System for Polynomial processing");
		frame.setSize(1200, 800);
		frame.setVisible(true);
		frame.setLayout(null);
		setLocationRelativeTo(null);

		txtInsertPolinom1 = new JTextField();
		txtInsertPolinom1.setBounds(100, 20, 400, 30);
		txtInsertPolinom1.setLayout(null);
		txtInsertPolinom1.setVisible(true);
		txtInsertPolinom1.setColumns(200);
		txtInsertPolinom1.setPreferredSize(new Dimension(200, 200));

		txtInsertPolinom2 = new JTextField();
		txtInsertPolinom2.setBounds(550, 20, 400, 30);
		txtInsertPolinom2.setLayout(null);
		txtInsertPolinom2.setVisible(true);
		txtInsertPolinom2.setColumns(200);
		txtInsertPolinom2.setPreferredSize(new Dimension(200, 200));
		txtInsertPolinom2.setColumns(90);

		frame.add(txtInsertPolinom1);
		frame.add(txtInsertPolinom2);

		displayAdd = new JLabel("ADD");
		displaySubstract = new JLabel("SUBSTRACT");
		displayMultiplication = new JLabel("MULTIPLY");
		displayDerivation = new JLabel("DERIVATE");
		displayIntegration = new JLabel("INTEGRATE");
		labelP = new JLabel("p");
		labelQ = new JLabel("q");
		labelError = new JLabel(" ");
		labelInstruction = new JLabel("Introduce the coefficients of the polynomials: eg. 6,2,4");

		frame.add(displayAdd);
		frame.add(displaySubstract);
		frame.add(displayMultiplication);
		frame.add(displayDerivation);
		frame.add(displayIntegration);
		frame.add(labelP);
		frame.add(labelQ);
		frame.add(labelError);
		frame.add(labelInstruction);

		labelInstruction.setBounds(300, 100, 400, 300);
		labelInstruction.setLayout(null);
		labelInstruction.setVisible(true);
		labelInstruction.setPreferredSize(new Dimension(800, 200));
		labelInstruction.setBorder(null);

		labelError.setBounds(300, 800, 400, 300);
		labelError.setLayout(null);
		labelError.setVisible(true);
		labelError.setPreferredSize(new Dimension(200, 200));
		labelError.setBorder(null);

		labelP.setBounds(80, 20, 400, 30);
		labelP.setLayout(null);
		labelP.setVisible(true);
		labelP.setPreferredSize(new Dimension(200, 200));
		labelP.setBorder(null);

		labelQ.setBounds(530, 20, 400, 30);
		labelQ.setLayout(null);
		labelQ.setVisible(true);
		labelQ.setPreferredSize(new Dimension(200, 200));
		labelQ.setBorder(null);

		displayAdd.setBounds(300, 150, 400, 330);
		displayAdd.setLayout(null);
		displayAdd.setVisible(true);
		displayAdd.setPreferredSize(new Dimension(200, 200));
		displayAdd.setBorder(null);

		displaySubstract.setBounds(300, 200, 400, 330);
		displaySubstract.setLayout(null);
		displaySubstract.setVisible(true);
		displaySubstract.setPreferredSize(new Dimension(200, 200));
		displaySubstract.setBorder(null);

		displayMultiplication.setBounds(300, 250, 400, 330);
		displayMultiplication.setLayout(null);
		displayMultiplication.setVisible(true);
		displayMultiplication.setPreferredSize(new Dimension(200, 200));
		displayMultiplication.setBorder(null);

		displayDerivation.setBounds(300, 300, 400, 330);
		displayDerivation.setLayout(null);
		displayDerivation.setVisible(true);
		displayDerivation.setPreferredSize(new Dimension(200, 200));
		displayDerivation.setBorder(null);

		displayIntegration.setBounds(300, 350, 400, 330);
		displayIntegration.setLayout(null);
		displayIntegration.setVisible(true);
		displayIntegration.setPreferredSize(new Dimension(200, 200));
		displayIntegration.setBorder(null);

		btnAddPolinom = new JButton("Add Polynom p and q");
		frame.add(btnAddPolinom);
		btnAddPolinom.setBounds(50, 100, 300, 20);

		btnSubstractPolinom = new JButton("Substract Polynom q from p");
		btnSubstractPolinom.setBounds(400, 100, 300, 20);
		frame.add(btnSubstractPolinom);

		btnMultiplyPolinom = new JButton("Multiply Polynom p and q");
		frame.add(btnMultiplyPolinom);
		btnMultiplyPolinom.setBounds(750, 100, 300, 20);

		btnDerivatePolinom = new JButton("Derivate Polynom p");
		frame.add(btnDerivatePolinom);
		btnDerivatePolinom.setBounds(50, 150, 300, 20);

		btnIntegratePolinom = new JButton("Integrate Polynom p");
		frame.add(btnIntegratePolinom);
		btnIntegratePolinom.setBounds(400, 150, 300, 20);

		btnAddPolinom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String s1, s2;
				int[] array1;
				int[] array2;
				String[] noOfCoeff1;
				String[] noOfCoeff2;
				int i;

				s1 = txtInsertPolinom1.getText();
				s2 = txtInsertPolinom2.getText();

				noOfCoeff1 = s1.split(",");
				noOfCoeff2 = s2.split(",");
				array1 = new int[noOfCoeff1.length];
				array2 = new int[noOfCoeff2.length];

				for (i = 0; i < noOfCoeff1.length; i++) {
					array1[i] = Integer.parseInt(noOfCoeff1[i]);
				}

				for (i = 0; i < noOfCoeff2.length; i++) {
					array2[i] = Integer.parseInt(noOfCoeff2[i]);
				}

				p1 = new Polinom(array1);
				p2 = new Polinom(array2);
				displayAdd.setText(p1.plusPolinom(p2).toString());
			}
		});

		btnSubstractPolinom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String s1, s2;
				int[] array1;
				int[] array2;
				String[] noOfCoeff1;
				String[] noOfCoeff2;
				int i;

				s1 = txtInsertPolinom1.getText();
				s2 = txtInsertPolinom2.getText();

				noOfCoeff1 = s1.split(",");
				noOfCoeff2 = s2.split(",");
				array1 = new int[noOfCoeff1.length];
				array2 = new int[noOfCoeff2.length];

				for (i = 0; i < noOfCoeff1.length; i++) {
					array1[i] = Integer.parseInt(noOfCoeff1[i]);

					for (i = 0; i < noOfCoeff2.length; i++) {
						array2[i] = Integer.parseInt(noOfCoeff2[i]);
					}

					p1 = new Polinom(array1);
					p2 = new Polinom(array2);
					displaySubstract.setText(p1.substractPolinom(p2).toString());
				}
			}
		});

		btnMultiplyPolinom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String s1, s2;
				int[] array1;
				int[] array2;
				String[] noOfCoeff1;
				String[] noOfCoeff2;
				int i;

				s1 = txtInsertPolinom1.getText();
				s2 = txtInsertPolinom2.getText();

				noOfCoeff1 = s1.split(",");
				noOfCoeff2 = s2.split(",");
				array1 = new int[noOfCoeff1.length];
				array2 = new int[noOfCoeff2.length];

				for (i = 0; i < noOfCoeff1.length; i++) {
					array1[i] = Integer.parseInt(noOfCoeff1[i]);
				}

				for (i = 0; i < noOfCoeff2.length; i++) {
					array2[i] = Integer.parseInt(noOfCoeff2[i]);
				}

				p1 = new Polinom(array1);
				p2 = new Polinom(array2);
				displayMultiplication.setText(p1.multiplyPolinom(p2).toString());

			}
		});

		btnDerivatePolinom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String s1;
				int[] array1;
				String[] noOfCoeff1;
				int i;

				s1 = txtInsertPolinom1.getText();
				noOfCoeff1 = s1.split(",");
				array1 = new int[noOfCoeff1.length];

				for (i = 0; i < noOfCoeff1.length; i++) {
					array1[i] = Integer.parseInt(noOfCoeff1[i]);
				}

				p1 = new Polinom(array1);

				displayDerivation.setText(p1.derivatePolinom(p1).toString());

			}
		});

		btnIntegratePolinom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String s1;
				int[] array1;
				String[] noOfCoeff1;
				s1 = txtInsertPolinom1.getText();
				int i;
				
				noOfCoeff1 = s1.split(",");
				array1 = new int[noOfCoeff1.length];

				for (i = 0; i < noOfCoeff1.length; i++) {
					array1[i] = Integer.parseInt(noOfCoeff1[i]);
				}

				p1 = new Polinom(array1);
				displayIntegration.setText(p1.integratePolinom(p1).toString());

			}
		});
	}
}
