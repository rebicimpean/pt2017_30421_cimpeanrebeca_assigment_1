package PT2017.demo.DemoProject;

public abstract class Monom {

	protected int grad;

	public abstract String toString();

	public abstract Monom plusMonom(Monom m);

	public abstract Monom multiplyMonom(Monom m);

	public abstract Monom derivateMonom(Monom m);

	public abstract Monom integrateMonom(Monom m);

}
