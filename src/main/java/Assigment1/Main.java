package PT2017.demo.DemoProject;

import java.awt.EventQueue;
//import java.awt.EventQueue;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class Main {

	public static void testApplication() {
		final Monom m1 = new IntMonom(4, 3);
		System.out.println("m1 =" + m1.toString());
		final Monom m2 = new IntMonom(4, 1);
		System.out.println("m2 =" + m2.toString());

		final int arraycoefficients1[] = { 1, 4, 0, 5, 3, 2 };
		final int arraycoefficients2[] = { 3, 5, 1, -5 };
		final int arraycoefficients3[] = { 1, 2 };
		final int arraycoefficients4[] = { 1, 2 };
		final Polinom p1 = new Polinom(arraycoefficients1);
		final Polinom p2 = new Polinom(arraycoefficients2);
		final Polinom p3 = new Polinom(arraycoefficients3);
		final Polinom p4 = new Polinom(arraycoefficients4);

		System.out.println("p1 = " + p1.toString());
		System.out.println("p2 = " + p2.toString());
		System.out.println(" ");
		final Polinom derivatePolinoms = ((Polinom) p1).derivatePolinom(p1);
		System.out.println("p1 derivat =  " + derivatePolinoms);
		final Polinom integratePolinoms = ((Polinom) p2).integratePolinom(p2);
		System.out.println("p2 integrat =  " + integratePolinoms);

		System.out.println("p2 = " + p2.toString());
		System.out.println(" ");

		final Polinom addPolinoms = p1.plusPolinom(p2);
		System.out.println("p1 + p2 =  " + addPolinoms);
		System.out.println(" ");

		final Polinom substractPolinoms = ((Polinom) p2).substractPolinom(p1);
		System.out.println("p2 - p1 =  " + substractPolinoms);

		final Polinom multiplyPolinoms = ((Polinom) p3).multiplyPolinom(p4);
		System.out.println("p3 * p4 =  " + multiplyPolinoms);

	}

	public static void main(String args[]) {

		// testApplication();

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PolinomGui a = new PolinomGui();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}