package PT2017.demo.DemoProject;

public class IntMonom extends Monom {

	private int coefficient;

	public int getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(int newCoefficient) {
		this.coefficient = newCoefficient;
	}
	
	public IntMonom(int grad, int coefficient) {
		this.grad = grad;
		this.coefficient = coefficient;
	}

	public Monom plusMonom(Monom m) {
		if (this.grad == m.grad)
			return (Monom) new IntMonom(grad, this.coefficient + ((IntMonom) m).coefficient);
		return null;
	}

	public Monom multiplyMonom(Monom m) {
		return (Monom) new IntMonom(this.grad + m.grad, this.coefficient * ((IntMonom) m).coefficient);
	}

	public Monom derivateMonom(Monom m) {
		if (grad - 1 >= 0)
			return (Monom) new IntMonom(this.grad - 1, this.coefficient);
		else
			return new IntMonom(0, 0);
	}

	public Monom integrateMonom(Monom m) {
		try {
			if ((this.grad + 1) != 0)
				return (Monom) new IntMonom(this.grad + 1, this.coefficient / (grad + 1));
		} catch (ArithmeticException e) {
		}
		return new IntMonom(0, 0);
	}

	public String toString() {
		String s = "";
		s = s + this.coefficient + "x^" + this.grad;
		return s;
	}
}
