package PT2017.demo.DemoProject;

public class DoubleMonom extends Monom {

	protected Double coefficient;

	public Double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(Double newCoefficient) {
		this.coefficient = newCoefficient;
	}

	public DoubleMonom(int grad, Double coefficient) {
		this.grad = grad;
		this.coefficient = coefficient;
	}

	public Monom plusMonom(Monom m) {
		if (this.grad == m.grad)
			return (Monom) new DoubleMonom(grad, this.coefficient + coefficient);
		return null;

	}

	public String toString() {
		String s = "";
		s = s + this.coefficient + "x^" + this.grad;
		return s;
	}

	public Monom integrateMonom(Monom m) {
		try {
			if ((this.coefficient / grad + 1) != 0)
				return (Monom) new DoubleMonom(this.grad + 1, ((DoubleMonom) this).coefficient / (grad + 1));

		} catch (ArithmeticException e) {
		}
		return new DoubleMonom(0, 0.0);
	}

	@Override
	public Monom multiplyMonom(Monom m) {
		return (Monom) new DoubleMonom(this.grad + m.grad, this.coefficient * ((DoubleMonom) m).coefficient);
	}

	@Override
	public Monom derivateMonom(Monom m) {
		if (grad - 1 >= 0)
			return (Monom) new DoubleMonom(this.grad - 1, this.coefficient);
		else
			return new DoubleMonom(0, 0.0);
	}

}
