package PT2017.demo.DemoProject;

import java.util.ArrayList;
import java.util.List;

public class Polinom {

	private List<Monom> monomList = new ArrayList<Monom>();
	private int grad;
	
	public int getGrad() {
		return grad;
	}

	public void setGrad(int newGrad) {
		this.grad = newGrad;
	}
	
	public List<Monom> getMonomList(){
		return monomList;
	}
	
	public Polinom() {

	}

	public Polinom(int[] a) {
		for (int i = 0; i < a.length; i++) {
			Monom e = new IntMonom(a.length - i - 1, a[i]);
			monomList.add(e);
			this.grad = a.length - 1;
		}
	}

	public String toString() {
		String s = "";
		for (Monom m : monomList) 
			if (((IntMonom) m).getCoefficient() != 0)
				s = s + "(" + ((IntMonom) m).toString() + ")" + "+";// adds each
																	// monom to
																	// string s
		if (s != null)
			s = s.substring(0, s.length() - 1); // delete the last plus

		return s;
	}

	// adds two polynoms
	public Polinom plusPolinom(Polinom p) {
		List<Monom> first = new ArrayList<Monom>();
		List<Monom> second = new ArrayList<Monom>();
		if (this.grad > p.grad) {
			first = this.monomList;
			second = p.monomList;
		} else {
			first = p.monomList;
			second = this.monomList;
		}

		Polinom rez = new Polinom();

		for (Monom e : first) {
			boolean found = false;
			for (Monom k : second)

				if (e.grad == k.grad) {
					rez.monomList.add(e.plusMonom(k));
					found = true;
				}

			if (found == false)
				rez.monomList.add(e);
		}

		return rez;
	}

	// substracts two polynoms
	public Polinom substractPolinom(Polinom p) {

		Polinom rez = this.plusPolinom(p.minusOne());
		return rez;
	}

	// used in substraction
	public Polinom minusOne() {
		for (Monom m : monomList){
			int a = ((IntMonom)m).getCoefficient();
			((IntMonom)m).setCoefficient(a* -1);
		}
		return this;
	}

	public Polinom multiplyPolinom(Polinom p) {
		Polinom res = new Polinom();
		res.grad = this.grad + p.grad;
		for (Monom m : this.monomList)
			for (Monom n : p.monomList)
				res.monomList.add(m.multiplyMonom(n));
		return res;
	}

	public Polinom derivatePolinom(Polinom p) {
		Polinom res = new Polinom();
		for (Monom m : p.monomList)
			res.monomList.add(m.derivateMonom(m));
		return res;

	}

	public Polinom integratePolinom(Polinom p) {
		Polinom res = new Polinom();
		for (Monom m : p.monomList)
			res.monomList.add(m.integrateMonom(m));
		return res;

	}
}
